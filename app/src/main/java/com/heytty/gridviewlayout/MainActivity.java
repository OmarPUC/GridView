package com.heytty.gridviewlayout;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<Cricketer> cricketers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.gridViewList);
        cricketers = new ArrayList<>();
        setData();
        CustomAdapter adapter = new CustomAdapter(MainActivity.this, cricketers);
        gridView.setAdapter(adapter);


    }

    private void setData() {
        Cricketer a = new Cricketer(R.drawable.rakitic, "Rakitic");
        Cricketer b = new Cricketer(R.drawable.cr7, "Ronaldo");
        Cricketer c = new Cricketer(R.drawable.mbappe, "Mbappe");
        Cricketer d = new Cricketer(R.drawable.coutinho, "Coutinho");
        Cricketer e = new Cricketer(R.drawable.sh75, "Shakib");
        cricketers.add(a);
        cricketers.add(b);
        cricketers.add(c);
        cricketers.add(d);
        cricketers.add(e);
        cricketers.add(a);
        cricketers.add(b);
        cricketers.add(c);
        cricketers.add(d);
        cricketers.add(e);
        cricketers.add(a);
        cricketers.add(b);
        cricketers.add(c);
        cricketers.add(d);
        cricketers.add(e);

    }
}
